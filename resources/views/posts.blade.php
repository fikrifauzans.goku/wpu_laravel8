
@extends('layouts.master')
@section('title')

<h1>Halaman Posts</h1>
<br>
@foreach ($posts as $post)
<br>
   <h3><a href="posts/{{ $post['slug'] }}">{{$post["title"]}}</a></h3>
   <strong>{{$post["author"]}}</strong>
   <p>{{$post["body"]}}</p>
@endforeach

@endsection
