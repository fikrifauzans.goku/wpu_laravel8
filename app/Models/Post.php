<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post
{
    private static $blog_posts  = [
    [
        "title" => "Judul Post Pertama",
        "slug" => "judul-post-pertama",
        "author" => "Fikri Fauzan",
        "body" => "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatibus, vero qui. Doloremque distinctio voluptates accusamus quo ipsam, totam amet ratione consequuntur rerum quaerat consequatur fugit fugiat sint quibusdam laborum mollitia odit dolor ut nam maxime? Est dignissimos a accusantium dolores error omnis quasi officia, rem cupiditate nobis exercitationem quas quaerat iure nihil! Voluptatem accusantium porro cupiditate recusandae natus rem nihil commodi totam labore, magnam consectetur cumque atque et accusamus eaque nostrum impedit nesciunt vitae corporis sit praesentium! Repellendus id placeat magni ducimus, explicabo sed corrupti dicta eius veniam ratione, consectetur possimus qui! Nesciunt saepe a repudiandae ab dolore perspiciatis accusamus!"
        ],
        [
        "title" => "Judul Post Kedua",
        "slug" => "judul-post-kedua",
        "author" => "Fadillah Hakim",
        "body" => "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatibus, vero qui. Doloremque distinctio voluptates accusamus quo ipsam, totam amet ratione consequuntur rerum quaerat consequatur fugit fugiat sint quibusdam laborum mollitia odit dolor ut nam maxime? Est dignissimos a accusantium dolores error omnis quasi officia, rem cupiditate nobis exercitationem quas quaerat iure nihil! Voluptatem accusantium porro cupiditate recusandae natus rem nihil commodi totam labore, magnam consectetur cumque atque et accusamus eaque nostrum impedit nesciunt vitae corporis sit praesentium! Repellendus id placeat magni ducimus, explicabo sed corrupti dicta eius veniam ratione, consectetur possimus qui! Nesciunt saepe a repudiandae ab dolore perspiciatis accusamus!"
        ],
        [
        "title" => "Judul Post Ketiga",
        "slug" => "judul-post-ketiga",
        "author" => "Rizal Nugraha",
        "body" => "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatibus, vero qui. Doloremque distinctio voluptates accusamus quo ipsam, totam amet ratione consequuntur rerum quaerat consequatur fugit fugiat sint quibusdam laborum mollitia odit dolor ut nam maxime? Est dignissimos a accusantium dolores error omnis quasi officia, rem cupiditate nobis exercitationem quas quaerat iure nihil! Voluptatem accusantium porro cupiditate recusandae natus rem nihil commodi totam labore, magnam consectetur cumque atque et accusamus eaque nostrum impedit nesciunt vitae corporis sit praesentium! Repellendus id placeat magni ducimus, explicabo sed corrupti dicta eius veniam ratione, consectetur possimus qui! Nesciunt saepe a repudiandae ab dolore perspiciatis accusamus!"
        ]

];

public static function all(){

    return collect(self::$blog_posts);

}

public static function find($slug){
    $post = static::all();
    return $post->firstWhere('slug' , $slug);
}


}
