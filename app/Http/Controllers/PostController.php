<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
   public function index(){
        return view('home',[
            "title" => "home"
        ]);


    }

    public function show() {
        return view('about', [
            "title" => "about",
            "name" => "Fikri Fauzan",
            "email" => "fikrifauzans.goku@gmail.com",
            "img" => "1"
        ]);
    }


    public function posts() {
        return view('posts' , [
            "title" => "posts",
            "posts" => Post::all()

        ]);
    }

    public function post($slug) {
            return view('post', [
                "title" => "Single Post",
                "post" => Post::find($slug)
            ]);
    }
}

